// Copyright 2022 The Pigweed Authors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

// Allows presubmit to fail on warnings.
#![cfg_attr(feature = "strict", deny(warnings))]
#![warn(clippy::pedantic)]

mod docmd;
mod graph;
mod hello;
mod subcommands;
mod target;

#[cfg(feature = "new_command")]
mod new;

#[cfg(feature = "python")]
mod py_demo;

use anyhow::Result;
use clap::{FromArgMatches, Subcommand};

use subcommands::Subcommands;

#[tokio::main]
async fn main() -> Result<()> {
    let app = clap::Command::new("qg");

    let app = Subcommands::augment_subcommands(app);

    let matches = app.get_matches();

    match Subcommands::from_arg_matches(&matches) {
        Ok(subcommands) => subcommands.run().await,
        Err(e) if e.kind() == clap::error::ErrorKind::MissingSubcommand => {
            run_main_command(&matches);
            Ok(())
        }
        Err(e) => e.exit(),
    }
}

fn run_main_command(_matches: &clap::ArgMatches) {
    println!("This is {}", qg::name());
}
