// Copyright 2022 The Pigweed Authors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

/// Says hello to a person.
#[derive(clap::Parser, Debug)]
#[command(about)]
pub struct Command {
    /// Use emoji output.
    #[arg(short, long)]
    emoji: bool,

    /// Name of the person to greet.
    #[arg(short, long)]
    name: String,
}

pub fn run(args: &Command) {
    let greeting = if args.emoji { "👋" } else { "Hello" };
    println!("{greeting}, {}!", args.name);
}
