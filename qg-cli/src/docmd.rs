// Copyright 2023 The Pigweed Authors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

use anyhow::Result;
use qg::Project;

/// Runs a single target in the project.
#[derive(clap::Parser, Debug)]
#[command(about)]
pub struct Command {
    /// Full name of the target to run.
    target: String,
}

pub async fn run(args: &Command) -> Result<()> {
    let project = Project::locate()?;
    project.run_target(&args.target).await?;

    println!("Target {} succeeded.", args.target);
    Ok(())
}
