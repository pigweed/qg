// Copyright 2022 The Pigweed Authors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

use anyhow::Result;
use qg::Project;

/// Manages packages in a project.
#[derive(clap::Parser, Debug)]
#[command(about)]
pub struct Command {}

// TODO(frolv): Actually use async.
#[allow(clippy::unused_async)]
pub async fn run(_args: &Command) -> Result<()> {
    let project = Project::locate()?;
    let registry = project.registry();

    println!(
        "Project {}, {} total targets",
        project.name(),
        registry.target_count()
    );

    for target in registry.targets() {
        println!("* {}", target.full_name());
        println!("  type: {}", target.type_string());
    }

    Ok(())
}
