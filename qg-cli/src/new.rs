// Copyright 2022 The Pigweed Authors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

use std::{fs, path::Path};

use anyhow::{bail, Result};
use qg::Project;

#[derive(clap::Parser, Debug)]
pub struct Command {
    /// The name of the project.
    name: String,
}

pub fn run(args: &Command) -> Result<()> {
    let root = Path::new(&args.name);
    if root.exists() {
        bail!(r#""{}" already exists in the current directory"#, args.name);
    }

    println!("Creating new project {}", args.name);
    fs::create_dir_all(root)?;

    Project::create(root, &args.name)?;

    Ok(())
}
