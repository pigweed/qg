// Copyright 2022 The Pigweed Authors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

use anyhow::Result;
use qg::Project;
use tokio::io::{self, AsyncWriteExt};

/// Dump the dependency graph in graphviz dot format.
#[derive(clap::Parser, Debug)]
#[command(about)]
pub struct Command {}

pub async fn run(_args: &Command) -> Result<()> {
    let project = Project::locate()?;
    let graph = project.registry().dot_dependency_graph();

    io::stdout().write_all(graph.as_bytes()).await?;
    Ok(())
}
