#!/bin/bash

set -e

USE_CIPD=

while [[ $# -gt 0 ]]; do
  case $1 in
    --cipd)
      USE_CIPD=1
      shift # past argument
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done

check() {
    echo "==> Running $@"
    "$@"
    echo "==="
}

# Get rust.
if [ -n "$USE_CIPD" ]; then
  CIPD_DIR="${TMPDIR:-/tmp}/qg-rust-${USER:-user}"
  mkdir -p $CIPD_DIR
  echo 'fuchsia/third_party/rust/${platform} git_revision:b7b7f2716ee1655a696d3d64c3e12638d0dd19c0' > "$CIPD_DIR/ensure"
  check cipd ensure -ensure-file "$CIPD_DIR/ensure" -root "$CIPD_DIR/install"
  export PATH="$CIPD_DIR/install/bin:$CIPD_DIR/install:$PATH"
fi

FEATURES=new_command,python,strict

# Build and test in release mode to make tests execute faster.
check cargo build --release --features ${FEATURES}
check cargo test --release --features ${FEATURES}
check cargo fmt --check
check cargo clippy --features ${FEATURES} -- -D clippy::pedantic -D warnings

echo
echo "All presubmit steps passed!"
