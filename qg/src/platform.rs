// Copyright 2022 The Pigweed Authors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

use std::str::FromStr;

use crate::Error;

#[derive(Debug)]
pub struct Platform {
    pub system: System,
    pub architecture: Architecture,
}

impl Platform {
    #[must_use]
    pub(crate) fn current() -> Self {
        Self::new(Self::target_system(), Self::target_architecture())
    }

    #[must_use]
    pub fn new(system: System, architecture: Architecture) -> Self {
        Platform {
            system,
            architecture,
        }
    }

    #[must_use]
    #[cfg(target_os = "linux")]
    fn target_system() -> System {
        System::Linux
    }

    #[must_use]
    #[cfg(target_os = "macos")]
    fn target_system() -> System {
        System::MacOs
    }

    #[must_use]
    #[cfg(target_os = "windows")]
    fn target_system() -> System {
        System::Windows
    }

    #[must_use]
    #[cfg(target_arch = "x86_64")]
    fn target_architecture() -> Architecture {
        Architecture::X64
    }

    #[must_use]
    #[cfg(any(
        all(target_arch = "arm", target_pointer_width = "64"),
        target_arch = "aarch64"
    ))]
    fn target_architecture() -> Architecture {
        Architecture::Arm64
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum System {
    Linux,
    MacOs,
    Windows,
}

impl FromStr for System {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "linux" => Ok(Self::Linux),
            "macos" => Ok(Self::MacOs),
            "windows" => Ok(Self::Windows),
            _ => Err(Error::GenericErrorPlaceholder),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Architecture {
    X64,
    Arm64,
}

impl FromStr for Architecture {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "x64" => Ok(Self::X64),
            "arm64" => Ok(Self::Arm64),
            _ => Err(Error::GenericErrorPlaceholder),
        }
    }
}
